/**
 * @author Thihara Neranjya
 * @author Impulse
 *
 * Plugin to validate the email recipient via a posgresql database.
 */

const pg = require('pg');
const util = require('util');

const constants = require('haraka-constants');


exports.register = function() {
  const plugin = this;
  plugin.logdebug("Initializing rcpt_to postgresql plugin.");
  const config = plugin.config.get('rcpt_to.psql.json');

  const dbConfig = {
    user: config.user,
    database: config.database,
    password: config.password,
    host: config.host,
    port: config.port,
    max: config.max,
    idleTimeoutMillis: config.idleTimeoutMillis
  };

  //Initialize the connection pool.
  plugin.pool = new pg.Pool(dbConfig);

  /**
   * If an error is encountered by a client while it sits idle in the pool the pool itself will emit an
   * error event with both the error and the client which emitted the original error.
   */
  plugin.pool.on('error', function(err, client) {
    plugin.logerror('Idle client error. Probably a network issue or a database restart.' +
      err.message + err.stack);
  });

  const domains = config.onlyDomains.split(',');
  plugin.onlyDomains = {};
  domains.forEach(domain => {
    plugin.onlyDomains[domain] = 1;
  });
  plugin.sqlQuery = config.sqlQuery;
};


exports.hook_rcpt = function(next, connection, params) {
  const plugin = this;
  const rcpt = params[0];

  this.logdebug("Checking validity of " + util.inspect(params[0]));

  let isValid = false;
  // if haraka is used as relay server, it becomes necessary to exclude all not known domains
  // otherwise it would block outgoing mails due to not being able to verify the recipient
  if (Object.keys(plugin.onlyDomains).length) {
    if (!plugin.onlyDomains[rcpt.host]) {
      connection.logdebug("Foreign domain. Continuing...", this);
      isValid = true;
      next();
    }
  }

  if (!isValid) this.is_user_valid(rcpt.user + '@' + rcpt.host, function(isValid) {
    if (isValid) {
      connection.logdebug("Valid email recipient. Continuing...", this);
      connection.transaction.results.add(plugin, {pass: 'rcpt_to_psql'});
      next();
    } else {
      connection.logdebug("Invalid email recipient. DENY email receipt.", this);
      connection.transaction.results.add(plugin, {fail: 'rcpt_to_psql'});
      next(constants.DENY, "Invalid email address.");
    }
  });
};


exports.shutdown = function() {
  this.loginfo("Shutting down validity plugin.");
  this.pool.end();
};


exports.is_user_valid = function(userID, callback) {
  const plugin = this;

  plugin.pool.connect(function(conErr, client, done) {
    if (conErr) {
      plugin.logerror('Error fetching client from pool. ' + conErr);
      return callback(false);
    }

    if (userID && userID.length) {
      client.query(plugin.sqlQuery,
        [userID.toLowerCase()],
        function(err, result) {

          //Release the client back to the pool by calling the done() callback.
          done();

          if (err) {
            plugin.logerror('Error running query. ' + err);
            return callback(false);
          }

          return callback(result.rows[0].exists);
        });
    } else {
      return callback(false);
    }
  });
};
