# haraka-plugin-rcpt-to-psql

Validates the rcpt_to addresses (recipient addresses) by connecting to a PostgreSQL database.
Excluding all but given domains from being checked. This plugin is based on the official haraka/haraka-plugin-rcpt-postgresql plugin.
Reason for this plugin: Since haraka does validate outgoing mail when haraka is used as a relay as if they were inbound mails, we
need a way to tell this plugin to ignore other target domains.

## Install
```
cd /my/haraka/config/dir
npm install git+https://implab.de/open/haraka-plugin-rcpt-to-psql#1.0.2
```

### Enable

Add the following line to the `config/plugins` file.

<!-- `rcpt-postgresql` -->
`haraka-plugin-rcpt-to-psql`

## Config

The `rcpt_to.psql.json` file has the following structure (defaults shown). Also note that this file will need to be created, if not present, in the `config` directory.

```javascript
{
  "user": "mailu",
  "database": "mailu",
  "password": "",
  "host": "127.0.0.1",
  "port": 5432,
  "max": 100,
  "idleTimeoutMillis": 30000,
  "onlyDomains": "remolutions.com,remofair.com,remolynx.com",
  "sqlQuery": "SELECT EXISTS(SELECT 1 FROM public.user WHERE email=LOWER($1)) AS \"exists\""
}
```
