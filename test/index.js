
const assert   = require('assert');
const fixtures = require('haraka-test-fixtures');

describe('haraka-plugin-rcpt-to-psql', function () {
    it('loads', function (done) {
        const plugin = new fixtures.plugin('haraka-plugin-rcpt-to-psql');
        // console.log(plugin);
        assert.ok(plugin);
        done();
    });

    it('registers', function (done) {
        const plugin = new fixtures.plugin('haraka-plugin-rcpt-to-psql');
        assert.equal(typeof plugin.register, 'function');
        plugin.register();
        assert.ok(plugin.sqlQuery);
        // console.log(plugin);
        done();
    });
});
